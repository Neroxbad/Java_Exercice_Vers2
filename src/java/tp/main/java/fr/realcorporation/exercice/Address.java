/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.exercice;

/**
 *
 * @author qpradet
 */

/* Exercice 3 */

public class Address {

    int addressid;
    private int numberstreet;
    private String champ1street;
    private String champ2street;
    private int zipcode;
    private String city;
    private String country;
    private float latitude;
    private float longitude;
    
    /* Alt + insert  pour crée un constructreur */
    
    
 /*constructeur 1 tout les paramétres */ 
    public Address(int addressid, int numberstreet, String champ1street, String champ2street, int zipcode, String city, String country, float latitude, float longitude) {
        this.addressid = addressid;
        this.numberstreet = numberstreet;
        this.champ1street = champ1street;
        this.champ2street = champ2street;
        this.zipcode = zipcode;
        this.city = city;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

  
    
     /*constructeur 2 Sans longitude et latitude */ 
    
    public Address(int addressid, int numberstreet, String champ1street, String champ2street, int zipcode, String city, String country) {
        this.addressid = addressid;
        this.numberstreet = numberstreet;
        this.champ1street = champ1street;
        this.champ2street = champ2street;
        this.zipcode = zipcode;
        this.city = city;
        this.country = country;
    }

    public Address() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getAddressid() {
        return addressid;
    }

    public void setAddressid(int addressid) {
        this.addressid = addressid;
    }

    public int getNumberstreet() {
        return numberstreet;
    }

    public void setNumberstreet(int numberstreet) {
        this.numberstreet = numberstreet;
    }

    public String getChamp1street() {
        return champ1street;
    }

    public void setChamp1street(String champ1street) {
        this.champ1street = champ1street;
    }

    public String getChamp2street() {
        return champ2street;
    }

    public void setChamp2street(String champ2street) {
        this.champ2street = champ2street;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
    
    /* Alt+insert */

    @Override
    public String toString() {
        return "address{" + "addressid=" + addressid + ", numberstreet=" + numberstreet + ", champ1street=" + champ1street + ", champ2street=" + champ2street + ", zipcode=" + zipcode + ", city=" + city + ", country=" + country + ", latitude=" + latitude + ", longitude=" + longitude + '}';
    }
    
    
    
    
    

}
