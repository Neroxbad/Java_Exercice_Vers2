/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.exercice;

/**
 *
 * @author qpradet
 * 
 * 
 */



/*exercice 4*/

public class Place {
    
      int placeid;
   
    private String nom ;
    private Address adresse;
    private Type type ;
    private String note;

    public Place() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Place(int id, String string, String string0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void addAddress(Address find) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
    public enum Type {
    
    Ecole , Cinéma , Théatre , Boowling 
    
    }

    public Place(int placeid, String nom, Address adresse, Type type, String note) {
        this.placeid = placeid;
        this.nom = nom;
        this.adresse = adresse;
        this.type = type;
        this.note = note;
    }

    public int getPlaceid() {
        return placeid;
    }

    public void setPlaceid(int placeid) {
        this.placeid = placeid;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Address getAdresse() {
        return adresse;
    }

    public void setAdresse(Address adresse) {
        this.adresse = adresse;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Place{" + "placeid=" + placeid + ", nom=" + nom + ", adresse=" + adresse + ", type=" + type + ", note=" + note + '}';
    }


    
    
    
}
