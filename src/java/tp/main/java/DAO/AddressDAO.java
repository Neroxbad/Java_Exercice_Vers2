/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import fr.realcorporation.exercice.Address;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
     /*exercice 9 */
/**
 *
 * @author Neroxbad
 */
public class AddressDAO extends DAO<Address> {

    public AddressDAO(Connection conn) {
        super(conn);
    }

    @Override
    public boolean create(Address obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Address obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Address obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Address find(int id) {

        Address address = new Address();

        try {
            ResultSet result = this.connect.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM address WHERE addressid = " + id);
            if (result.first()) {
                address = new Address(id,result.getInt("numberstreet"), result.getString("champ1street")
                        , result.getString("champ2street"), result.getInt("zipcode")
                        , result.getString("city"), result.getString("country")
                        ,result.getFloat("latitude") ,result.getFloat("longitude") );  
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return address;
    }

}


     /*exercice 9 */