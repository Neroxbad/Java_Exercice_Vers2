/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import fr.realcorporation.exercice.Place;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
     /*exercice 9 */
/**
 *
 * @author Neroxbad
 */
public class PlaceDAO extends DAO<Place> {

    public PlaceDAO(Connection conn) {
        super(conn);
    }

    @Override
    public boolean create(Place obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Place obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Place obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Place find(int id) {
        
   Place place = new Place();      
      
    try {
      ResultSet result = this.connect.createStatement(
        ResultSet.TYPE_SCROLL_INSENSITIVE,
        ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM place WHERE placeid = " + id);
      if(result.first())
        place = new Place(id,result.getString("nom") , result.getString("note"));
      
       result.beforeFirst();
        AddressDAO matDao = new AddressDAO(this.connect);
            
        while(result.next())
          place.addAddress(matDao.find(result.getInt("addressid"))); 
  
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return place;
  }
    
}
     /*exercice 9 */